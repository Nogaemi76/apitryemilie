
const express = require('express');
const axios = require('axios');
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });
const URL = 'http://localhost:8081';
const webserver = express();

webserver.use(express.static(__dirname + '/public'));
//affiche le formulaire sur localhost:8080/user/new
webserver.set('view engine','ejs');

webserver.get('/user/new',function (req,res) {
    res.render('pages/form');
});

//récupère les données du formulaire avec la méthode POST pour les envoyer vers l'api
webserver.post('/user/new', urlencodedParser, function (req, res){
    // res.send('quelque chose se passe....');
    console.log(req.body);

    //envoi de req.body sur l'api (localhost:8081)
    axios.post(URL + '/user/new',req.body )
        .then(function (response) {
            console.log('youhou');
            // res.send('quelque chose se passe....');
            //cette phrase s'affiche sur localhost:8080/user/new
            res.render('pages/form');
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
            res.end();
        });

});

webserver.get('/user', function (req,res){
    axios.get(URL + '/user')
        .then(function(response){
            console.log(response.data);
            let myUsersList = response.data;
            res.render('pages/user_list', { myUsersList });

        })
        .catch(function(error){
           console.log(error);
        });
});
webserver.listen(8080);
